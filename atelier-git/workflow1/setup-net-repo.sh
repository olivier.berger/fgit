#!/bin/sh

# Choose a home for repositories to be shared
repodir=../../../repodir

# Init the 'repo1' repo (bare)
git init --bare $repodir/repo1

# Populate that repo through filesystem remote
git clone $repodir/repo1

cd repo1

git commit --allow-empty -m "empty initial commit"
git checkout -b test
git branch -d master

echo "obergix was here"> README.txt

git add README.txt
git commit -m "Hi mom" README.txt
git push origin test

cd ..
rm -fr repo1

# Allow export of the repo
touch $repodir/repo1/git-daemon-export-ok

# Share the repo(s) allowing pushes
cd $repodir
git daemon --base-path=. --export-all --reuseaddr --informative-errors --verbose --enable=receive-pack

# The repo is ready for : 'git clone git://localhost/repo1' or 'git clone git://157.159.110.64/repo1'

