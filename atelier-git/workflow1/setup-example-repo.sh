#!/bin/sh

git init testrepo

cd testrepo

echo "Example repository for the fgit workshop"> README.txt
git add README.txt
git commit -a -m "1. Initial commit"

cat >> README.txt <<EOF

This illustrates the structure of a git tree containing several revisions.

-- Olivier Berger
EOF

cat > COPYRIGHT <<EOF

This work is Copyright 2014 Olivier Berger - Institut Mines Telecom, and is placed under the MIT license.

EOF
git add COPYRIGHT
git commit -a -m "2. More details + adding a license"

cat > README.txt <<EOF
Example repository for the fgit workshop

This illustrates the structure of a git tree containing several revisions.

See LICENSE for terms and conditions.

-- Olivier Berger
EOF

git mv COPYRIGHT LICENSE
git commit -a -m "3. better naming for the file"


cat > hello.py <<EOF
#!/usr/bin/python

if __name__ == '__main__':
	print "hello world"

EOF
git add hello.py

git commit -a -m "4. first piece of code"

rev2=$(git log --oneline -3 --reverse | head -n 1 | cut -f 1 -d ' ')
git checkout -b devel $rev2

cat > tools.py <<EOF

def foo(bar):
    return bar

EOF
git add tools.py
git commit -a -m "5. prepare some code for the future"

cat > tools.py <<EOF
#!/usr/bin/python

def foo(bar):
    return bar

if __name__ == '__main__':
    import sys
    if foo('bar') != 'bar':
        print >>sys.stderr, "error"
        sys.exit(1)
EOF

git commit -a -m "6. add a test"

git checkout master
git checkout -b feature_X
rev3=$(git log --oneline -2 --reverse | head -n 1 | cut -f 1 -d ' ')
git branch -d master
git checkout -b master $rev3

cat > LICENSE <<EOF

This work is Copyright (c) 2014 Olivier Berger - Institut Mines Telecom

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

EOF

cat > README.txt <<EOF
Example repository for the fgit workshop
----------------------------------------

This illustrates the structure of a git tree containing several revisions.

See LICENSE for terms and conditions.

-- Olivier Berger
EOF

git commit -a -m "7. full text of the license"

git merge -m "8. merging feature_X" feature_X

# git branch -d feature_X

# or

git checkout feature_X
git merge --no-ff -m "9. syncing with master" master

cat > hello.py <<EOF
#!/usr/bin/python

def hello(arg):
	return 'hello ' + arg

if __name__ == '__main__':
	print hello('world')

EOF
git commit -a -m "10. using a function"
