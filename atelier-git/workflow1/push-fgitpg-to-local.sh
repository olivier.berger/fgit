#!/bin/sh

set -e

git clone git://localhost/repo1

cd repo1

git remote add fusionforge git+ssh://obergix@fusionforge.int-evry.fr//var/lib/gforge/chroot/scmrepos/git/fgitpg/fgitpg.git
git fetch fusionforge
git checkout -b master fusionforge/master
git push origin master
git branch devel fusionforge/devel
#git branch feature_X fusionforge/feature_X
git push origin devel
#git push origin feature_X

